SELECT
  t1.item_id,
  t1.item_name,
  t1.item_price,
  t.category_name
FROM
  item_category t
INNER JOIN
  item t1
ON
  t.category_id = t1.category_id
